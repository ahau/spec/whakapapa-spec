# Whakapapa spec

Whakapapa in Ahau are made up of the following record-types:
1. **Profiles** for people:
    - `profile/person`
    - `profile/person/admin`
2. **Relationships** between profiles:
    - `link/profile-profile/child`
    - `link/profile-profile/partner`
3. **Views**, which describe where to start drawing a tree, which direction, profiles to exclude etc.
    - `whakapapa/view`

These are handled by [ssb-profile] (1) and [ssb-whakapapa] (2, 3)


```mermaid
graph TB

%% view
view((view))

%% profiles
Grandad(Grandad)
Dad(Dad)
Mum(Mum)
Son(Son)
Daughter(Daughter)

%% focus
view-. focus .-Grandad

%% child links
GrandadDad{{ child }}
DadSon{{ child }}
DadDaughter{{ child }}
MumDaughter{{ child }}

Grandad---GrandadDad-->Dad
Dad---DadSon-->Son
Dad---DadDaughter-->Daughter
Mum---MumDaughter-->Daughter

%% partner links
DadMum{{ partner }}

DadMum---Dad
DadMum---Mum


%% styling
classDef default fill: #e953da, stroke-width: 0, color: white
classDef edgePath stroke: #142f48
classDef edgeLabel background: #fff
classDef LS-DadMum stroke: #1e8cb0

classDef view fill: #4c44cf, stroke-width: 0, color: white
classDef childLink fill: #142f48, stroke-width: 0, color: white
classDef partnerLink fill: #1e8cb0, stroke-width: 0, color: white

class view view
class GrandadDad,DadSon,DadDaughter,MumSon,MumDaughter childLink
class DadMum partnerLink
```

```mermaid
%% key
graph LR

subgraph key
  view((View))-.-labelV[whakapapa/view]-.-descV[defines the focus - where to start drawing<br > and some other rules, like how to resolve duplication]

  Name(Name)-.-labelP[profile/person]-.-descP[a profile, containing preferredName, avatarImage,...]

  childLink{{child}}-.-labelCL[link/profile-profile/child]-.-descCL[link pointing from parent profile -> child profile<br /> also stores meta-data on relationshipType e.g. birth/adopted]

  partnerLink{{partner}}-.-labelPL[link/profile-profile/partner]-.-descPL[*undirected* link between two profiles that are partners]
end

classDef default fill: #e953da, stroke-width: 0, color: white

classDef view fill: #4c44cf, stroke-width: 0, color: white
classDef childLink fill: #142f48, stroke-width: 0, color: white
classDef partnerLink fill: #1e8cb0, stroke-width: 0, color: white

class view view
class childLink childLink
class partnerLink partnerLink

%% styles - key-only
classDef label fill: none, stroke-width: 0
classDef msgType fill: white, stroke-width: 0
class descV,descP,descCL,descPL label
class labelV,labelP,labelCL,labelPL msgType
```






## Kaitiaki-only whakapapa

A kaitiaki-only whakapapa is one which exists solely inside the admin-subgroup of a group.

This means all records are encrypted to the groupId of the subgroup.


```mermaid
graph TB

subgraph group
  GroupGrandad(Grandad)
  GrandadGrandad{{admin}}

  subgraph admin-group
    %% view
    view((view))

    %% profiles
    Grandad(Grandad)
    Dad(Dad)
    Mum(Mum)
    Son(Son)
    Daughter(Daughter)

    %% child links
    GrandadDad{{ child }}
    DadSon{{ child }}
    DadDaughter{{ child }}
    MumDaughter{{ child }}

    %% partner links
    DadMum{{ partner }}
  end

  Grandad---GrandadGrandad-->GroupGrandad
end

view-. focus .-Grandad

Grandad---GrandadDad-->Dad
Dad---DadSon-->Son
Dad---DadDaughter-->Daughter
Mum---MumDaughter-->Daughter


DadMum---Dad
DadMum---Mum


%% styling
classDef default fill: #e953da, stroke-width: 0, color: white
classDef edgePath stroke: #142f48
classDef edgeLabel background-color: #ffffffaa
classDef cluster fill: #f5c1f033, stroke: #f5c1f0, stroke-width: 2
classDef LS-DadMum stroke: #1e8cb0

classDef view fill: #4c44cf, stroke-width: 0, color: white
classDef childLink fill: #142f48, stroke-width: 0, color: white
classDef partnerLink fill: #1e8cb0, stroke-width: 0, color: white

class view view
class GrandadDad,DadSon,DadDaughter,MumSon,MumDaughter,GrandadGrandad childLink
class DadMum partnerLink
```


Note that some profiles may have associated profiles in the parent group - here we show an "unowned" profile in the group (not linked to any device feedId) which has been linked to an associated kaitiaki-only profile in the admin subgroup.

### Building a kaitiaki-only whakapapa

When starting a whakapapa, kaitaiki will be presented with the option for the whakapapa to be "admin only".

When they are searching for profiles they would like to add to such a whakapapa they will search for results from:
- the admin only subgroup (prefer these - i.e. don't show admin + linked parent group profile if they both exist!)
- the parent group
 
Depending one what results are found (if any), and where they are found you may end up on one of the following scenarios:


#### **1. Linking to a profile already in the admin-only group**

```mermaid
graph LR

subgraph group
  subgraph admin-group[admin group]
    %% profiles
    Dad(Dad)
    Daughter(Daughter)

    %% child links
    DadDaughter{{ child }}:::invisible
    Dad-.-DadDaughter-.-Daughter
  end
end

%% styling
classDef default fill: #e953da, stroke-width: 0, color: white
classDef cluster fill: #f5c1f033, stroke: #f5c1f0, stroke-width: 2

classDef newLink fill: none, stroke: #142f48, stroke-width: 2, color: #142f48
classDef invisible fill: none, stroke: none, color: #00000000

classDef edgePath stroke: none
```
_We search for "Daughter" to add as a child to Dad, and find and existing admin profile._

Note: there might be an associated group profile, but it's not shown here.

```mermaid
graph LR

subgraph group
  subgraph admin-group[admin group]
    %% profiles
    Dad(Dad)
    Daughter(Daughter)

    %% child links
    DadDaughter{{ child }}:::newLink
    Dad-.-DadDaughter-.->Daughter
  end
end

%% styling
classDef default fill: #e953da, stroke-width: 0, color: white
classDef cluster fill: #f5c1f033, stroke: #f5c1f0, stroke-width: 2

classDef newLink fill: none, stroke: #142f48, stroke-width: 2, color: #142f48
```
_We make a "child" link in the admin group_


#### **2. Linking to a profile in the parent group**

```mermaid
graph LR

subgraph group
  GroupDaughter(Daughter)

  subgraph admin-group
    %% profiles
    Dad(Dad)
    Daughter(Daughter):::invisible

    %% child links
    DadDaughter{{child}}:::invisible
  end

  %% admin links
  DaughterDaughter{{admin}}:::invisible

  Dad-.-DadDaughter-.-Daughter
  Daughter-.-DaughterDaughter-.-GroupDaughter
end


%% styling
classDef default fill: #e953da, stroke-width: 0, color: white
classDef cluster fill: #f5c1f033, stroke: #f5c1f0, stroke-width: 2

classDef edgePath stroke: none
classDef invisible fill: none, stroke: none, color: #00000000
```
_We search for “Daughter” to add as a child to Dad, and don't find an admin-profile, but find a group profile._

```mermaid
graph LR

subgraph group
  GroupDaughter(Daughter)

  subgraph admin-group
    %% profiles
    Dad(Dad)
    Daughter(Daughter):::newProfile

    %% child links
    DadDaughter{{child}}:::newLink
  end

  %% admin links
  DaughterDaughter{{admin}}:::newLink

  Dad-.-DadDaughter-.->Daughter
  Daughter-.-DaughterDaughter-.->GroupDaughter
end


%% styling
classDef default fill: #e953da, stroke-width: 0, color: white
classDef cluster fill: #f5c1f033, stroke: #f5c1f0, stroke-width: 2

classDef newProfile fill: none, stroke: #e953da, stroke-width: 2,  color: #e953da
classDef newLink fill: none, stroke: #142f48, stroke-width: 2, color: #142f48
```
_We create an admin profile for the Daughter, then create the child link to that (as well as an admin-link connecting the "group profile" and "admin profile")_

#### **3. Linking to a brand-new profile**

You don't find any Daughter profile you want to link to (in either admin-group nor group).

```mermaid
graph LR

subgraph group
  GroupDaughter(Daughter):::invisible

  subgraph admin-group
    %% profiles
    Dad(Dad)
    Daughter(Daughter):::invisible

    %% child links
    DadDaughter{{child}}:::invisible
  end

  %% admin links
  DaughterDaughter{{admin}}:::invisible

  Dad-.-DadDaughter-.-Daughter
  Daughter-.-DaughterDaughter-.-GroupDaughter
end


%% styling
classDef default fill: #e953da, stroke-width: 0, color: white
classDef cluster fill: #f5c1f033, stroke: #f5c1f0, stroke-width: 2

classDef invisible fill: none, stroke: none, color: #00000000
classDef edgePath stroke:none
```

You have two options:

##### **a) create an admin-only profile**

This is useful in the scenario where perhaps you do no want anyone in the general group to know about "Daughter"

```mermaid
graph LR

subgraph group
  GroupDaughter(Daughter):::invisible

  subgraph admin-group
    %% profiles
    Dad(Dad)
    Daughter(Daughter):::newProfile

    %% child links
    DadDaughter{{child}}:::newLink
  end

  %% admin links
  DaughterDaughter{{admin}}:::invisible

  Dad-.-DadDaughter-.-Daughter
  Daughter-.-DaughterDaughter-.-GroupDaughter
end


%% styling
classDef default fill: #e953da, stroke-width: 0, color: white
classDef cluster fill: #f5c1f033, stroke: #f5c1f0, stroke-width: 2

classDef newProfile fill: none, stroke: #e953da, stroke-width: 2,  color: #e953da
classDef newLink fill: none, stroke: #142f48, stroke-width: 2, color: #142f48

classDef invisible fill: none, stroke: none, color: #00000000
classDef LS-DaughterDaughter stroke: none
classDef LE-DaughterDaughter stroke: none
```

##### **b) create an admin + group profile**

Maybe it's ok for the group to know about the existence of the Daughter, it's just the child link which might not be for the group to know about.

```mermaid
graph LR

subgraph group
  GroupDaughter(Daughter):::newProfile

  subgraph admin-group
    %% profiles
    Dad(Dad)
    Daughter(Daughter):::newProfile

    %% child links
    DadDaughter{{child}}:::newLink
  end

  %% admin links
  DaughterDaughter{{admin}}:::newLink

  Dad-.-DadDaughter-.->Daughter
  Daughter-.-DaughterDaughter-.->GroupDaughter
end


%% styling
classDef default fill: #e953da, stroke-width: 0, color: white
classDef cluster fill: #f5c1f033, stroke: #f5c1f0, stroke-width: 2

classDef newProfile fill: none, stroke: #e953da, stroke-width: 2,  color: #e953da
classDef newLink fill: none, stroke: #142f48, stroke-width: 2, color: #142f48
```

---

#### NOTES

- A. it's possible to make an admin profile with no associated group profile
    - should we ask the user to confirm anything here?
- B. if you create an "unowned" profile in the admin only space, then that user registers...
    - PROBLEM: registration creates an admin-only profile. This may be a duplicate of an existing unowned profile
    - paths to this:
        - in (2) you're pulling an "unowned" profile into the admin only space
        - you search for a profile, don't find a match and just create a new profile
    - Solutions:
      - delete the unowned profile from whakapapa, and manually connected the new owned one
      - detect the duplicate during registration, and handle it automatically
          - tombstone + redirect?
- C. we don't allow whakapapa links (child, partner) to profiles in parent groups, because ... we think that would lead to problems later
    - e.g. wanting to save some admin-only details...
    - which group should the link be encrypted to? the current standard is to encrypt to the child... but then that could reveal a birth relationship that is only for kaitiaki to know
- D. perhaps the kaitiaki-only whakakpapa should have a different type?
    - e.g. `whakapapa/view/admin`
    - we've seen with profiles that using the same type in different spaces can cause problems. It might be prudent to diverge now. Worst case there is some small redundancy

